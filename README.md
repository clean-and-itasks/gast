# G∀ST

This repository contains the G∀ST testing framework for Clean.

An overview of the library and the underlying ideas is provided in:

* ftp://ftp.cs.ru.nl/pub/CSI/SoftwEng.FunctLang/papers/2006/koop2006-AutomaticTestingFunctionSpecificationsCEFPS.pdf
* https://www.researchgate.net/publication/235900230_Model_Based_Testing_with_Logical_Properties_versus_State_Machines

## G∀ST's special status concerning dependencies

G∀ST is used to test many libraries in the Clean ecosystem.
So if a library introduces a breaking change in a function that is both tested
by G∀ST _and_ used by G∀ST this is problematic.

- Be careful when exporting types 
- NEVER export generic functions (they cannot be masked using `qualified`
  imports).

## License

The package is licensed under the BSD-2-Clause license; for details, see the [LICENSE](LICENSE) file.
