implementation module Data.Map.Gast

import Control.GenBimap
import qualified Data.Map
from Data.Map import :: Map
import StdEnv

import Gast.Gen, Gast.GenLibTest

ggen{|Map|} _ _ _ = abort "test data generation for maps cannot be implemented"
genShow{|Map|} fx fy sep p x rest = genShow{|* -> *|} (genShow{|* -> * -> *|} fx fy) sep p ('Data.Map'.toList x) rest
