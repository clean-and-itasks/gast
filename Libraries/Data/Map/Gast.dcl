definition module Data.Map.Gast

from Data.Map import :: Map

from Gast.Gen import generic ggen, :: GenState
from Gast.GenLibTest import generic genShow

derive ggen Map
derive genShow Map
