definition module Data.Error.Gast

from Data.Error import :: MaybeError

from Gast.Gen import generic ggen, :: GenState
from Gast.GenLibTest import generic genShow

derive ggen MaybeError
derive genShow MaybeError
