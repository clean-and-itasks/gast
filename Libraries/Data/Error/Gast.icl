implementation module Data.Error.Gast

import Control.GenBimap
from Data.Error import :: MaybeError
import StdEnv

import Gast.Gen, Gast.GenLibTest

derive ggen MaybeError
derive genShow MaybeError
