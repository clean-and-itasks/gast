definition module Gast

/*
	GAST: A Generic Automatic Software Test-system

	Pieter Koopman, 2004-2008
	Radboud Universty, Nijmegen
	The Netherlands
	pieter@cs.ru.nl
*/

import Gast.GenLibTest, Gast.Testable, Gast.StdProperty
from Gast.Gen import generic ggen, :: GenState {..}, genState, aStream, ggenString, :: PairTree, :: Mode,
	derive ggen UNIT PAIR EITHER CONS OBJECT RECORD FIELD Int Bool Real Char String
		() (,) (,,) (,,,) (,,,,) (,,,,,) (,,,,,,) (,,,,,,,) (,,,,,,,,) [] [!] [ !] [!!] {} {!} ? ?^ ?# Either

class Gast a | ggen{|*|}, genShow{|*|}, gPrint{|*|} a
