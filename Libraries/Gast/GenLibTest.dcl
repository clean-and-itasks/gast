definition module Gast.GenLibTest

/*
	GAST: A Generic Automatic Software Test-system

	Gast.GenLibTest: library for generic testing: showing and comparing values

	Pieter Koopman, 2004
	Radboud Universty, Nijmegen
	The Netherlands
	pieter@cs.ru.nl
*/

from Data.Either import :: Either
from Data.Set import :: Set
import StdGeneric

import StdClass
import _SystemStrictMaybes

(@@) infixl 2 :: !(a->b)  a -> b
(@@!)infixl 2 :: !(a->b) !a -> b

/**
 * Generic printing function.
 *
 * @param separator.
 * @param parenthesis required.
 * @param value to print.
 * @param accumulator.
 * @result output.
 */
generic genShow a :: !String !Bool !a ![String] -> [String]
generic gLess a :: a a -> Bool

derive genShow Int, Char, Bool, Real, String,
	UNIT, PAIR, EITHER, OBJECT, CONS of {gcd_name,gcd_arity}, RECORD of {grd_name}, FIELD of {gfd_name},
	{}, {!}, [], [!], [ !], [!!], (->), ?, ?^, ?#,
	(), (,), (,,), (,,,), (,,,,), (,,,,,), (,,,,,,), (,,,,,,,), (,,,,,,,,), (,,,,,,,,,), Set, Either
derive gLess Int, Char, Bool, Real, String,
	UNIT, PAIR, EITHER, OBJECT, CONS, FIELD, RECORD,
	[], ?, ?^, ?#,
	(,), (,,), (,,,), (,,,,), (,,,,,), (,,,,,,), (,,,,,,,), (,,,,,,,,), (,,,,,,,,,)

show  :: !a -> [String] | genShow{|*|} a
show1 :: !a ->  String  | genShow{|*|} a

(-<-) infix 4 :: !a !a -> Bool | gLess{|*|} a
(->-) infix 4 :: !a !a -> Bool | gLess{|*|} a
(-<=) infix 4 :: !a !a -> Bool | gLess{|*|} a
(=>-) infix 4 :: !a !a -> Bool | gLess{|*|} a
