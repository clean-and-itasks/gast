definition module Text.Unicode.Encodings.UTF8.Gast

from Gast.Gen import generic ggen, :: GenState
from Gast.GenLibTest import generic genShow
from Text.Unicode.Encodings.UTF8 import :: UTF8

derive genShow UTF8
derive ggen UTF8