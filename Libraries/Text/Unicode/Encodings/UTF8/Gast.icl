implementation module Text.Unicode.Encodings.UTF8.Gast

import Control.GenBimap
import StdEnv

import Gast.Gen, Gast.GenLibTest
from Text.Unicode.Encodings.UTF8 import :: UTF8

derive genShow UTF8
derive ggen UTF8
