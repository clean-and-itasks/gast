definition module Text.Unicode.UChar.Gast

from Gast import generic genShow, generic ggen, :: GenState
from Text.Unicode.UChar import :: UChar

derive genShow UChar
derive ggen    UChar
