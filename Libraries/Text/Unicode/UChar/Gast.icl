implementation module Text.Unicode.UChar.Gast

import StdInt
import Gast, Gast.Gen
import Text.Unicode.UChar

genShow{|UChar|} sep p uchar rest = genShow{|*|} sep p (toChar uchar) rest

ggen{|UChar|} _ =
	interleave
		range1
		(interleave
			range2
			(interleave
				range3
				(interleave
					range4
					(interleave range5
						(interleave range6
							(interleave range7
								(interleave range8 range9)
							)
						)
					)
				)
			)
		)
where
	// The values belonging to the ranges below are decoded differently in UTF8
	// Using this to generate data makes sure to include values representing all of these classes.
	range1 = [!fromInt c \\ c <- [0x00..0x7f]]
	range2 = [!fromInt c \\ c <- [0x80..0x07ff]]
	range3 = [!fromInt c \\ c <- [0x0800..0x0fff]]
	range4 = [!fromInt c \\ c <- [0x1000..0xcfff]]
	range5 = [!fromInt c \\ c <- [0xd000..0xd7ff]]
	range6 = [!fromInt c \\ c <- [0xe000..0xffff]]
	range7 = [!fromInt c \\ c <- [0x10000..0x3ffff]]
	range8 = [!fromInt c \\ c <- [0x40000..0xfffff]]
	range9 = [!fromInt c \\ c <- [0x100000..0x10FFFF]]
