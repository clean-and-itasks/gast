definition module Text.GenJSON.Gast

from Gast import generic genShow, generic ggen, :: GenState
from Text.GenJSON import :: JSONNode

derive genShow JSONNode
derive ggen JSONNode
