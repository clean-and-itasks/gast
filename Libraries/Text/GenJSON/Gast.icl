implementation module Text.GenJSON.Gast

import Control.GenBimap
import Gast
import StdEnv
import Text.GenJSON

import Gast.Gen

derive genShow JSONNode
derive ggen JSONNode
