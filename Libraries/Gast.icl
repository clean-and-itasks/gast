implementation module Gast

/*
	GAST: A Generic Automatic Software Test-system

	Pieter Koopman, 2004
	Radboud Universty, Nijmegen
	The Netherlands
	pieter@cs.ru.nl
*/
import Gast.GenLibTest, Gast.Testable, Gast.StdProperty
import Gast.Gen
