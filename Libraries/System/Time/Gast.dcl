definition module System.Time.Gast

from Gast import generic ggen, generic genShow, :: GenState
from Text.GenPrint import generic gPrint, class PrintOutput, :: PrintState

from System.Time import :: Timespec

derive ggen Timespec
derive genShow Timespec
derive gPrint Timespec
