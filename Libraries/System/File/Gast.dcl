definition module System.File.Gast

from System.File import :: FileError
from Gast import generic ggen, generic genShow, :: GenState

derive genShow FileError
derive ggen FileError
