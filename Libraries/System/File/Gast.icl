implementation module System.File.Gast

import Control.GenBimap
import StdEnv, System.File

import Gast.Gen, Gast.GenLibTest

derive genShow FileError
derive ggen FileError
