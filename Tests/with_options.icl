module with_options

/**
 * An example program using the Gast.CommandLine module to wrap a test
 * collection in a CLI application.
 * Compile this with -nr -nt. For usage details, see --help on the executable.
 *
 * The tests are taken from Peter Achten's StdSetTest in
 * https://gitlab.science.ru.nl/peter88/FP_Example_Solutions/.
 */

import Data.Set
import StdBool
from StdFunc import flip
import StdList => qualified filter, insert
import StdString
import StdOrdList

import Control.GenBimap
from Data.Func import $
import Data.GenLexOrd
import Text.GenPrint

import Gast, Gast.CommandLine, Gast.Gen

Start w = exposeProperties [OutputTestEvents] []
	[ EP membership
	, EP conversion_invariant
	, EP length_correct
	, EP subset_correct
	, EP proper_subset_correct
	, EP newSet_is_empty
	, EP emptyset
	]
	w

:: Enum = A | B | C
derive class Gast Enum
derive gEq Enum
derive gLexOrd Enum
instance == Enum where (==) x y = x === y
instance <  Enum where (<)  x y = (x =?= y) === LT

membership :: Enum [Enum] -> Property
membership x xs = member x (fromList xs) <==> isMember x xs

conversion_invariant :: [Enum] -> Bool
conversion_invariant xs = xs` == fromList (lazyList $ toList xs`)
where xs` = fromList xs

length_correct :: [Enum] -> Bool
length_correct xs = size (fromList xs) == length (removeDup xs)

subset_correct :: [Enum] [Enum] -> Property
subset_correct xs ys = isSubsetOf (fromList xs) (fromList ys)
	<==> all (flip isMember ys) xs

proper_subset_correct :: [Enum] [Enum] -> Property
proper_subset_correct xs ys = isProperSubsetOf (fromList xs) (fromList ys)
	<==> all (flip isMember ys) xs && not (all (flip isMember xs) ys)

newSet_is_empty :: Property
newSet_is_empty = name "newSet_is_empty" $ null newSet

emptyset :: [Enum] -> Property
emptyset xs =
	(size xs` == 0 <==> null xs`) /\
	(xs` == newSet <==> null xs`)
where xs` = fromList xs

lazyList :: ![a] -> [a]
lazyList x = x
