module bool

import StdEnv
import Text.GenPrint

import Gast, Gast.Gen

Start = quiet aStream \x y.(x && y) == not (not x || not y)
