module char

import StdEnv
import Text.GenPrint

import Gast, Gast.Gen

pChar c = isAlpha c || toUpper c == c

Start = quiet aStream pChar
