# Changelog

#### 0.4.20

- Enhancement: derive ggen for tuples that contain up to 15 elements.

#### 0.4.19

- Enhancement: redo changes of 0.4.17 but remove support for base 2.0.

#### 0.4.18

- Fix: revert changes of 0.4.17 (this breaks compatibility with base 2.0).

#### 0.4.17

- Enhancement: import all `ggen` instances defined in `Gast.Gen` and all `gPrint` instances defined in `Text.GenPrint`
               when importing `Gast`.

#### 0.4.16

- Chore: support base `3.0`.

#### 0.4.15

- Feature: add `ggen` and `genShow` derivations for `:: FileError`.

#### 0.4.14

- Feature: add `ggen` and `genShow` derivations for `:: JSONNode`.

#### 0.4.13

- Feature: add `genShow` derivation for `:: Map`.

#### 0.4.12

- Enhancement: Reduce the heap usage of `ggen`. Heap usage was higher than with version `2`, but is now even slightly
               lower.

#### 0.4.11

- Feature: add the `genShow` and `ggen` derivations for `:: Either`.

#### 0.4.10

- Feature: add `Data.Error.Gast` module, containing the `genShow` and `ggen` derivation for `:: MaybeError`.

#### 0.4.9

- Chore: remove constaint on base-compiler.

#### 0.4.8

- Feature: move `Text.Unicode.Encodings.UTF8.Gast` to the gast package from the text package
           to avoid a circular dependency.
- Feature: Add derivation of `genShow` for `:: Set`.
- Feature: move `Text.Unicode.UChar.Gast` to the gast package from the text package
           to avoid a circular dependency.

#### 0.4.7

- Chore: require system 2 and support generic-print 1 and 3 instead of 1 and 2.
- Feature: move `System.Time.Gast` to the gast package.

#### 0.4.6

- Chore: support generic-print 2 alongside generic-print 1.

#### 0.4.5

- Chore: support compiler 3.1 (fixed issues with the run time system).

#### 0.4.4

- Enhancement: `ggen` generates the minimal/maximal values for `Char`.

#### 0.4.3

- Fix: restrict `base-compiler < 3.0.3`, as higher versions cause segfaults.

#### 0.4.2

- Enhancement: drop local dependencies and use packages now.

#### 0.4.1

- Fix: add library files for windows to `nitrile.yml` to fix packaging.

### 0.4.0

- Change: add derives for builtin maybe types.
- Change: remove some occurances of gEq.

#### 0.3.1

- Fix: fix published packages; provide separate packages for Linux-x64, Linux-x86 and Windows-x64.

## 0.3.0

- Remove dependency on clean-platform by importing the dependencies in the tree.
- Change: `Gast.ConfSM` is not imported anymore with `Gast`.

#### 0.2.3

- Chore: accept clean-platform 0.3 and 0.4 as dependency.

#### 0.2.2

- Enhancement: derive ggen for 9-tuples.

#### 0.2.1

- Chore: update platform dependency version to 0.3.1.

## 0.2.0

- Switch to nitrile.

## 22.5

No changes

## 22.4

No changes

## 22.3

No changes

## 22.2

No changes

## 22.1

No changes

## 22.0

No changes

## 21.7

No changes

## 21.6

### Fixes
- `quietn`/`quiet` now print CE including arguments; `verbosePrintConfig` includes printing CE args, Gijs Alberts (@gijstop), !54

## 21.5

No changes

## 21.4

### Fixes
- fix with_options.icl which was broken by recent change in Data.Set API, Steffen Michels (@smichels), !49

## 21.3

No changes

## 21.2

- Added license

## 21.1

First tagged release
